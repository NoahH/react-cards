import React from 'react';
import Card from './Card.js';
import CardGroup from './CardGroup.js';

const App = () => (
    <CardGroup>
        <Card description = "Trial!" price = "Free!" icon = "fa-thumbs-o-up" />
        <Card description = "Basic tier" price = "$10.00" icon = "fa-trophy" hint="most popular"/>
        <Card description = "Advanced Tier" price = "$6,000.00" icon = "fa-bolt" hint="only for enterprise-level professionals"/>
    </CardGroup>    
);

export default App;
